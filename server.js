const express = require("express");

const app = express();
const PORT = 5001;
app.use(express.json());

let userRoutes = require("./app/routes.js");
app.use("/users", userRoutes);
app.use("/login", userRoutes);

app.listen(PORT, () => console.log(`Running on PORT ${PORT}`));
